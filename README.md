# S3app Team

Данный репозиторий содержит документацию проекта "Протопии". Документы содержаться в папке src.

**Авторы и участники внесшие значительный вклад**: [Список авторов и участников внесшие значительный вклад](/contributors.md)

**version:** ***0.0.1***

***CC-BY-SA***

Если у вас есть замечания, предложения к команде проекта, то вы можете добавить issues (обсуждения) в **[список задач](https://gitlab.com/protopia-foundation/protopia-foundation/-/issues)**

## Contribute

1. Установите `Typora`
2. Установите `git` 
3. Склонируйте репозиторий командой 
4. Откройте любой файл и внесите изменения
5. Сделайте `git commit`
6. Дождитесь подтверждения от mainteiner
7. Ваш вклад стал частью общего дела



##  Build Instructions

With Docker Compose:

```bash
# Site:
$ docker-compose run --rm foliant make site
# PDF:
$ docker-compose run --rm foliant make pdf
```

With pip and stuff (requires Python 3.6+, Pandoc, and TeXLive):

```bash
$ pip install -r requirements.txt
# Site:
$ foliant make site
# PDF:
$ foliant make pdf
```
